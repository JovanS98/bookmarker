# Project Bookmarker

Internet baza podataka za knjige, gde korisnici mogu da se registruju, loguju na nalog, ocenjuju knjige, konektuju se sa drugim korisnicima, dobijaju preporuke, analiziraju statistike itd.

# Build

- [Build](https://gitlab.com/matfpveb/projekti/2020-2021/14-Bookmarker/-/wikis/Build)

## Developers

- [Marija Radovic, 269/2017](https://gitlab.com/MarijaRadovic)
- [Nevena Mesar, 107/2015](https://gitlab.com/nevena-m)
- [Jovan Stamenkovic, 148/2017](https://gitlab.com/JovanS98)
- [Nikola Jovanovic, 78/2017](https://gitlab.com/LuciusVorenus98)
