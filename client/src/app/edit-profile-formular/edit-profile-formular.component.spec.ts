import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditProfileFormularComponent } from './edit-profile-formular.component';

describe('EditProfileFormularComponent', () => {
  let component: EditProfileFormularComponent;
  let fixture: ComponentFixture<EditProfileFormularComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EditProfileFormularComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EditProfileFormularComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
