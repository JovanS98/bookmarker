import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { ReactiveFormsModule} from '@angular/forms';
import { CKEditorModule } from '@ckeditor/ckeditor5-angular';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { HeaderComponent } from './header/header.component';
import { ErrorPageComponent } from './error-page/error-page.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { SlideshowComponent } from './slideshow/slideshow.component';
import { FooterComponent } from './footer/footer.component';
import { RegisterComponent } from './register/register.component';
import { BookComponent } from './book/book.component';
import { AddStoryComponent } from './add-story/add-story.component';
import { StoriesComponent } from './stories/stories.component';
import { AddBookComponent } from './add-book/add-book.component';
import { LoginService } from './login/login.service';
import { AddAuthorComponent } from './add-author/add-author.component';
import { BooksComponent } from './books/books.component';

import { SocialLoginModule, SocialAuthServiceConfig } from 'angularx-social-login';
import { GoogleLoginProvider } from 'angularx-social-login';
import { AuthorComponent } from './author/author.component';
import { SearchComponent } from './search/seach/seach.component';
import { SimpleSearchComponent } from './search/simple-search/simple-search.component';
import { FilterFieldsComponent } from './search/filter-fields/filter-fields.component';
import { FilterFieldComponent } from './search/filter-field/filter-field.component';
import { BookmChipComponent } from './search/bookm-chip/bookm-chip.component';
import { ResultCardComponent } from './search/result-card/result-card.component';

import { ScrollingModule } from '@angular/cdk/scrolling';
import { ProbaComponent } from './proba/proba.component';
import { MatDialogModule } from '@angular/material/dialog';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { EditProfileFormularComponent } from './edit-profile-formular/edit-profile-formular.component';
import { ClickOutsideModule } from 'ng-click-outside';
import { AdminSectionComponent } from './admin-section/admin-section.component';
import { AuthInterceptor } from './services/auth.interceptor';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    HeaderComponent,
    ErrorPageComponent,
    DashboardComponent,
    SlideshowComponent,
    FooterComponent,
    RegisterComponent,
    BookComponent,
    AddStoryComponent,
    StoriesComponent,
    AddBookComponent,
    AddAuthorComponent,
    BooksComponent,
    AuthorComponent,
    SearchComponent,
    SimpleSearchComponent,
    FilterFieldsComponent,
    FilterFieldComponent,
    BookmChipComponent,
    ResultCardComponent,
    ProbaComponent,
    EditProfileFormularComponent,
    AdminSectionComponent
  ],
  imports: [
    HttpClientModule,
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule,
    CKEditorModule,
    SocialLoginModule,
    ScrollingModule,
    MatDialogModule,
    BrowserAnimationsModule,
    ClickOutsideModule
  ],
  providers: [LoginService,
    {
      provide: 'SocialAuthServiceConfig',
      useValue: {
        autoLogin: false,
        providers: [
          {
            id: GoogleLoginProvider.PROVIDER_ID,
            provider: new GoogleLoginProvider(
              '136767401151-8rgdso02o543u60u9usi86uduub2fb2q.apps.googleusercontent.com'
            )
          }
        ]
      } as SocialAuthServiceConfig,
    },
    // {
    //   provide : HTTP_INTERCEPTORS,
    //   useClass: AuthInterceptor,
    //   multi   : true,
    // },
    DashboardComponent],
  bootstrap: [AppComponent],
  entryComponents: [ProbaComponent]
})
export class AppModule { }
