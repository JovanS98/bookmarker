import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ErrorPageComponent } from './error-page/error-page.component';
import { LoginComponent } from './login/login.component';
import { SlideshowComponent } from './slideshow/slideshow.component';
import { RegisterComponent } from './register/register.component';
import { BookComponent } from './book/book.component';
import { AddStoryComponent } from './add-story/add-story.component';
import { StoriesComponent } from './stories/stories.component';
import { AddBookComponent } from './add-book/add-book.component';
import { AddAuthorComponent } from './add-author/add-author.component';
import { BooksComponent } from './books/books.component';
import { AuthorComponent } from './author/author.component';
import { SearchComponent } from './search/seach/seach.component';
import { ProbaComponent } from './proba/proba.component';
import { EditProfileFormularComponent } from './edit-profile-formular/edit-profile-formular.component';
import { AdminSectionComponent } from './admin-section/admin-section.component';

const routes: Routes = [
  { path: "" , component: SlideshowComponent },
  { path: "error", component: ErrorPageComponent},
  { path: "dashboard", component: DashboardComponent},
  { path: "dashboard/:idUser", component: DashboardComponent},
  { path: "slideshow" , component: SlideshowComponent },
  { path: "register" , component: RegisterComponent},
  { path: "login" , component: LoginComponent },
  { path: "book/:idBook", component: BookComponent},
  { path: "addStory", component: AddStoryComponent},
  { path: "stories", component: StoriesComponent},
  { path: "stories/:id", component: StoriesComponent},
  { path: "addBook", component: AddBookComponent},
  { path: "addAuthor", component: AddAuthorComponent},
  { path: "books", component: BooksComponent},
  { path: "author/:id", component: AuthorComponent},
  { path: "search", component: SearchComponent},
  { path: "proba", component: ProbaComponent},
  { path: "editProfileFormular", component: EditProfileFormularComponent},
  { path: "adminSection", component: AdminSectionComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
