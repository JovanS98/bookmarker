import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Story } from '../models/story.model';
import { User } from '../models/user.model';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  private usersSource = new BehaviorSubject<[User]>(null)
  users = this.usersSource.asObservable();

  constructor() { }

  changeUsers(users: any) {
    this.usersSource.next(users);
  }

}
