import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { Observable } from 'rxjs';
import { JwtService } from '../common/services/jwt.service';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

    constructor(private jwtService: JwtService){
        console.log("napravio authinterc ", jwtService.getToken());
        
    }

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        req = req.clone({
        setHeaders: {
            'Content-Type' : 'application/json; charset=utf-8',
            'Accept'       : 'application/json',
            'Authorization': `${this.jwtService.getToken()}`,
        },
    });

    return next.handle(req);
  }
}