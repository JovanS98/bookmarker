import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ReviewService {

  private readonly reviewUrl = 'http://localhost:3000/review/';

  constructor(private http: HttpClient) { }

  getUserReviews (userId: string) {
    return this.http.get(this.reviewUrl + 'user/' + userId);
  }

  getFollowingReviews(idUser: any){
    return this.http.get(this.reviewUrl + 'following/' + idUser);
  }
}
