import { Component, OnInit } from '@angular/core';

export type AdminAction = "book"|"author"|"users";

@Component({
  selector: 'app-admin-section',
  templateUrl: './admin-section.component.html',
  styleUrls: ['./admin-section.component.css']
})
export class AdminSectionComponent implements OnInit {

  selectedAction: AdminAction;
 
  constructor() { }

  ngOnInit(): void {

  }

  onAction(adminAction: AdminAction){
    this.selectedAction=adminAction;
  }

}
