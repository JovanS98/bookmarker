import { UserRole } from "src/app/models/user.model";

export interface IJWTTokenData {
    id: string;
    username: string;
    email: string;
    name: string;
    surname: string;
    image: string;
    following: [string];
    followers: [string];
    age: number;
    place: string;
    role?: UserRole
}