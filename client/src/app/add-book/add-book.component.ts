declare var require: any

import { AfterViewInit, Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AddBookService } from './add-book.service';
import { AddAuthorService } from '../add-author/add-author.service'
import { async } from '@angular/core/testing';
import { AuthorService } from '../author/author.service';
import { promise } from 'selenium-webdriver';

var FormData = require('form-data');
declare const M: any;

@Component({
  selector: 'app-add-book',
  templateUrl: './add-book.component.html',
  styleUrls: ['./add-book.component.css']
})
export class AddBookComponent implements OnInit {
  public checkoutForm: FormGroup;
  image: any;
  resultAuthors: any;
  authors: any;
  finAuthors: any;
  author: any
  wantedAuthorName: any;

  constructor(private formBuilder: FormBuilder, private addBookService: AddBookService,
    private addAuthorService: AddAuthorService,
    private authorService: AuthorService) {

    this.checkoutForm = this.formBuilder.group({
      title: [''],
      author: [''],
      genres: [''],
      description: [''],
      coverImagePath: ['']
    });
  }
  ngOnInit(): void {

    this.fetchAuthors();

    M.updateTextFields();

    this.addBookService.getAllBooks().subscribe(books => {
      console.log(books);
    });
  }

  public selectImage(event: any) {
    if (event.target.files.length > 0) {
      const file = event.target.files[0];
      this.image = file;
    }

  }

  public submitForm(data: any) {
    const formData = new FormData();
    formData.append('image', this.image);
    for (var value of formData.values()) {
      console.log(value);
    }

    console.log("COMPONENT.TS");
    console.log(data);

    this.wantedAuthorName = data.author;

    console.log("Wanted author");
    console.log(this.wantedAuthorName);

    for (let i = 0; i < this.resultAuthors.length; i += 1) {
      var currentAuthor = this.resultAuthors[i];
      if (currentAuthor.name == this.wantedAuthorName) {
        this.author = currentAuthor;
      }
    }

    console.log("I catched this author:");
    console.log(this.author);

    formData.append('title', data.title);
    formData.append('author', this.author._id);
    formData.append('genres', data.genres);
    formData.append('description', data.description);

    this.addBookService.createImage(formData, data).subscribe((book: any) => {
      window.alert('Book is successfully created!');
      this.checkoutForm.reset();
    });

  }


  public initDropDown() {
    var elems = document.querySelectorAll('select');
    console.log(elems)
    var instances = M.FormSelect.init(elems);
  }

  public fetchAuthors() {
    this.addAuthorService.getAllAuthors().subscribe(authors => {
      this.resultAuthors = authors;
      console.log(this.resultAuthors);
      setTimeout(this.initDropDown, 100)
    });

  }

}
