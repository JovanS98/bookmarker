import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AddStoryService {

  private readonly storiesUrl = 'http://localhost:3000/stories/add';

  constructor(private http: HttpClient) { }

  postStory(data : any){
    const body = {...data}
    return this.http.post<any>(this.storiesUrl, body)
  }
}
