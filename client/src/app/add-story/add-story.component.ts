import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';
import { AuthService } from '../services/auth.service';
import { AddStoryService } from './add-story.service';

@Component({
  selector: 'app-add-story',
  templateUrl: './add-story.component.html',
  styleUrls: ['./add-story.component.css']
})
export class AddStoryComponent implements OnInit {
  public storyForm: FormGroup;
  private sub: Subscription;
  public loggedInUser: any = null;

  constructor(private formBuilder: FormBuilder,
    private addStoryService: AddStoryService,
    private authService: AuthService) {

    this.sub = this.authService.user.subscribe((user) => {
      this.loggedInUser = user;
    });

    this.authService.sendUserDataIfExists();

    this.storyForm = this.formBuilder.group({
      storyTitle: ['', [Validators.required]],
      storyContent: ['', [Validators.required]]
    });
  }

  ngOnInit(): void {

  }

  public submitForm(data: any) {

    if(!this.storyForm.valid)
      return;

    data['storyAuthor'] = this.loggedInUser.id;

    this.addStoryService.postStory(data).subscribe((story: any) => {
      window.alert('Story is successfully created!');
      window.location.reload();
      console.log(story);
    });
  }

  public reset() {
    this.storyForm.reset();
  }

}
