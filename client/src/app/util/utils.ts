import * as moment from "moment-timezone";

export function findPath(path: String) {
    try{
        return "http://localhost:3000/" + path.replace('\\', '/');
    }
    catch(err){
        console.log(`[find path] '${path}' `, err);
    }
    return undefined
}

export function getPath(route: string[]){
    return "http://localhost:3000/"+route.join("/");
}

export function isEmpty(value: any):boolean{
    if(value === undefined || value ==null || String(value) === ""){
      return true;
    }
    return false;
}


//--------------- Date utils
export const MOMENT_DATE_FORMAT = "DD.MM.YYYY"; // 01.01.2021
export const MOMENT_DATE_FORMAT_NO_ZERO = "D.M.YYYY"; // 1.1.2021
export const MOMENT_LOCALE = "Europe/Belgrade";

export function getDate(dateString: string) : moment.Moment {
    let date: moment.Moment = moment.tz(dateString, MOMENT_LOCALE);
    if(date.isValid()){
        return date;
    }
    date = moment.tz(dateString, MOMENT_DATE_FORMAT, true, MOMENT_LOCALE);
    if(date.isValid()){
        return date;
    }
    return moment.tz(dateString, MOMENT_DATE_FORMAT_NO_ZERO, true, MOMENT_LOCALE);
}
