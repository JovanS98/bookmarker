import { Observable, throwError } from 'rxjs';
import { HttpErrorResponse } from '@angular/common/http';
import { Router } from '@angular/router';

export abstract class HttpErrorHandler {
  constructor(protected router: Router) {}

  protected handleError() {
    return (error: HttpErrorResponse): Observable<never> => {
      if (error.error instanceof ErrorEvent) {
        console.error('An error occurred:', error.error.message);
      } else {
        if (error.error) {
          //todo obrada greske
        }

        this.router.navigate([
          '/error',
          { message: error.error.errorMessage, statusCode: error.status },
        ]);
      }
      return throwError('Something bad happened; please try again later.');
    };
  }
}
