import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthorService {

  constructor(private http: HttpClient) { }

  private readonly authorUrl = "http://localhost:3000/author/";
  private readonly reviewUrl = "http://localhost:3000/review/"

  public getAuthor(idAuthor: any) : Observable<any>{
    return this.http.get<any>(this.authorUrl + idAuthor);
  }

  public getReviews(data: any) : Promise<any>{
    return this.http.get<any>(this.reviewUrl + data).toPromise();
  }
}
