import { Component, OnInit } from '@angular/core';
import { AuthorService } from './author.service';
import { ActivatedRoute } from '@angular/router';

declare const M: any;

@Component({
  selector: 'app-author',
  templateUrl: './author.component.html',
  styleUrls: ['./author.component.css']
})
export class AuthorComponent implements OnInit {

  theAuthor: any;
  authorsBooks: any;

  ratings: any = [];

  paramMapSub: any;
  idAuthor: any;

  constructor(private authorService: AuthorService,
    private route: ActivatedRoute) {

    this.paramMapSub = this.route.paramMap.subscribe(params => {
      this.idAuthor = params.get('id');

      this.authorService.getAuthor(this.idAuthor).subscribe((book: any) => {
        this.theAuthor = [book];
      });
    });
  }

  ngOnInit(): void {
    this.showAuthor();
  }

  public findPath(path: String) {
    return "http://localhost:3000/" + path.replace('\\', '/');
  }

  public showAuthor() {
    this.authorService.getAuthor(this.idAuthor).subscribe(async (data: any) => {
      this.theAuthor = [data];
      this.authorsBooks = data.books;
      await this.getRatings();
    });
  }

  public findYear(year: any) {
    if (year)
      return year.toString();
    else
      return "";
  }

  public findNextBookRoute(bookId: String) {
    return '/book/' + bookId;
  }

  public async getRatings() {
    for (var i = 0; i < this.authorsBooks.length; i++) {
      await this.authorService.getReviews(this.authorsBooks[i]._id).then((data: any) => {
        this.ratings.push(data);
      });
    }
    this.calculateAvg();
  }

  public calculateAvg() {
    for (var i = 0; i < this.authorsBooks.length; i++) {
      var num = 0;
      var sum = 0;
      for (var j = 0; j < this.ratings[i].length; j++) {
        if (this.ratings[i][j].reviews.rating > 0) {
          num++;
          sum += this.ratings[i][j].reviews.rating;
        }
      }
      if (num > 0) {
        this.authorsBooks[i].avg = sum / num;
      }
      else {
        this.authorsBooks[i].avg = 0;
      }
    }
  }
}
