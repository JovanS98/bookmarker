import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import { catchError, tap } from 'rxjs/operators';
import { HttpErrorHandler } from '../util/http-error-handler.model';
import { Observable } from 'rxjs';
import { User } from '../models/user.model';
import { environment } from 'src/environments/environment';

import { SocialAuthService, GoogleLoginProvider, SocialUser } from 'angularx-social-login';

export interface msg {
  success: boolean;
  message: string;
}

@Injectable({
  providedIn: 'root'
})
export class LoginService extends HttpErrorHandler {

  private readonly loginUrl = 'http://localhost:3000/auth/';
  public message: msg;
  private loggedInStatus = false;
  loggedInWithGoogle = false;
  
  socialUser: SocialUser;

  constructor(router: Router,
    private http: HttpClient,
    private socialAuthService: SocialAuthService) { 
      super(router);
    }
    

    loginWithGoogle(): void {
      console.log("ulogovan sa googlom");

      this.socialAuthService.signIn(GoogleLoginProvider.PROVIDER_ID);
      this.setLoggedIn(true);
      this.loggedInWithGoogle = true;
    }
  
    logOutGoogle(): void {
      this.socialAuthService.signOut();
      this.setLoggedIn(false);
      this.loggedInWithGoogle = false;
    }

    setLoggedIn(value: boolean){
      this.loggedInStatus = value;
    }

    get isLoggedIn() {
      return this.loggedInStatus;
    }

    logout() {
      if(this.loggedInWithGoogle){
        this.socialAuthService.signOut();
      }
      else{
          localStorage.removeItem('access_token');
      } 
    }

  }

