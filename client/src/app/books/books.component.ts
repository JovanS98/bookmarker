import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { BooksService } from './books.service';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { User } from '../models/user.model';
import { AuthService } from '../services/auth.service';

declare const M: any;

@Component({
  selector: 'app-books',
  templateUrl: './books.component.html',
  styleUrls: ['./books.component.css']
})
export class BooksComponent implements OnInit {

  private sub: Subscription;
  public loggedInUser: User = null;

  public applySortForm: FormGroup;
  public applyFilterForm: FormGroup;
  public allBooks: any;
  public allReviews: any;

  public allGenres: any = ["Fantasy", "Adventure", "Romance", "Contemporary", "Dystopian", "Mystery", "Horror", "Thriller", "Paranormal",
    "Historical fiction", "Science fiction", "Memoir", "Cooking", "Art", "Self-help / Personal", "Development", "Motivational",
    "Health", "History", "Travel", "Guide / How-to", "Families & Relationships", "Humor", "Children’s", "Realist Novel", "Southern Gothic",
    "Bildungsroman"];

  public displayBooks: any = [];

  numbers: any = [];

  constructor(private formBuilder: FormBuilder,
    private booksService: BooksService,
    private route: ActivatedRoute,
    private authService: AuthService) {

    
    this.applySortForm = this.formBuilder.group({
      option: ['']
    });
    this.applyFilterForm = this.formBuilder.group({
      option: ['']
    });

    this.sub = this.authService.user.subscribe((user: User) => {
      this.loggedInUser = user;
    });

    this.authService.sendUserDataIfExists();
  }

  ngOnInit(): void {
    document.addEventListener('DOMContentLoaded', function () {
      var elems = document.querySelectorAll('select');
      var instances = M.FormSelect.init(elems);
    });

    this.getBooks();
  }

  public findPath(path: String) {
    return "http://localhost:3000/" + path.replace('\\', '/');
  }

  public applySort(data: any) {
    console.log(this.displayBooks);
    switch (data.option) {
      case "":
        return;
      case "nameAsc":
        this.displayBooks =[...this.displayBooks.sort(function (d1: any, d2: any) {
          var t1 = d1.title;
          var t2 = d2.title;
          if (t1 >= t2)
            return 1;
          else
            return -1
        })];
        break;
      case "nameDesc":
        this.displayBooks =[...this.displayBooks.sort(function (d1: any, d2: any) {
          var t1 = d1.title;
          var t2 = d2.title;
          if (t1 <= t2)
            return 1;
          else
            return -1
        })];
        break;
      case "authorAsc":
        this.displayBooks =[...this.displayBooks.sort(function (d1: any, d2: any) {
          var t1 = d1.author.name;
          var t2 = d2.author.name;
          if (t1 >= t2)
            return 1;
          else
            return -1
        })];
        break;
      case "authorDesc":
        this.displayBooks =[...this.displayBooks.sort(function (d1: any, d2: any) {
          var t1 = d1.author.name;
          var t2 = d2.author.name;
          if (t1 <= t2)
            return 1;
          else
            return -1
        })];
        break;
      case "ratingAsc":
        this.displayBooks =[...this.displayBooks.sort(function (d1: any, d2: any) {
          var t1 = d1.avg;
          var t2 = d2.avg;
          if (t1 >= t2)
            return 1;
          else
            return -1
        })];
        break;
      case "ratingDesc":
        this.displayBooks =[...this.displayBooks.sort(function (d1: any, d2: any) {
          var t1 = d1.avg;
          var t2 = d2.avg;
          if (t1 <= t2)
            return 1;
          else
            return -1
        })];
        break;
      default:
        return;
    }
  }

  public applyFilter(data: any) {
    if (data.option) {
      this.displayBooks = [];
      for (var i = 0; i < this.allBooks.length; i++) {
        var book = this.allBooks[i];
        var filtered = true;
        for (var j = 0; j < data.option.length; j++) {
          var option = data.option[j];

          if (option === ">4.5") {
            if (book.avg < 4.5) {
              filtered = false;
            }
          }
          if (option === ">3.5") {
            if (book.avg < 3.5) {
              filtered = false;
            }
          }
          if (option === ">2.5") {
            if (book.avg < 2.5) {
              filtered = false;
            }
          }
          if (option === ">1.5") {
            if (book.avg < 1.5) {
              filtered = false;
            }
          }
          if (option === "notRated") {
            if (this.loggedInUser) {
              if (book.logrev) {
                filtered = false;
              }
            }
          }
          if (option === "rated") {
            if (this.loggedInUser) {
              if (!book.logrev) {
                filtered = false;
              }
            }
          }

          var genres;
          genres = (book.genre + '').split(",");
          for (var l = 0; l < this.allGenres.length; l++) {
            if (option === this.allGenres[l]) {
              var tmp = false;
              for (var k = 0; k < genres.length; k++) {
                if (genres[k] === this.allGenres[l]) {
                  tmp = true;
                }
              }
              if (!tmp) {
                filtered = false;
              }
            }
          }

        }
        if (filtered) {
          this.displayBooks.push(book);
        }
      }
    }

  }

  public getBooks() {
    this.booksService.getBooks().subscribe((data: any) => {
      this.allBooks = data;
      this.displayBooks = this.allBooks;
      this.getReviews();
    });
  }

  public getReviews() {
    this.booksService.getReviews().subscribe((data: any) => {
      this.allReviews = data;
      this.calculateAvg();
    });
  }

  public calculateAvg() {
    for (var i = 0; i < this.allBooks.length; i++) {
      var num = 0;
      var sum = 0;
      this.allBooks[i].logrev = false;
      for (var j = 0; j < this.allReviews.length; j++) {
        if (this.allReviews[j].book == this.allBooks[i]._id && this.allReviews[j].reviews.rating > 0) {
          num++;
          sum += this.allReviews[j].reviews.rating;
        }
        //da li je trenutni korisnik ocenio
        if (this.allReviews[j].book == this.allBooks[i]._id && this.allReviews[j].reviews.user?._id == this.loggedInUser?.id) {
          this.allBooks[i].logrev = true;
        }
      }
      if (num > 0)
        this.allBooks[i].avg = sum / num;
      else
        this.allBooks[i].avg = 0;
    }
  }

}
