import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class BooksService {

  private readonly booksUrl = 'http://localhost:3000/books';
  private readonly reviewsUrl = 'http://localhost:3000/review';

  constructor(private http: HttpClient) { }

  public getBooks() : Observable<any>{
    return this.http.get<any>(this.booksUrl);
  }

  public getReviews() : Observable<any>{
    return this.http.get<any>(this.reviewsUrl);
  }
}
