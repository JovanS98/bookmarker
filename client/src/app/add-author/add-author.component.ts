declare var require: any

import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AddAuthorService } from './add-author.service';

var FormData = require('form-data');

declare const M: any;

@Component({
  selector: 'app-add-author',
  templateUrl: './add-author.component.html',
  styleUrls: ['./add-author.component.css']
})
export class AddAuthorComponent implements OnInit {

  public checkoutForm: FormGroup;
  image: any;

  constructor(private formBuilder: FormBuilder, private addAuthorService: AddAuthorService) {

    this.checkoutForm = this.formBuilder.group({
      name: [''],
      bio: [''],
      imagePath: [''],
      nationality: [''],
      born: [''],
      died: [''],
      books: ['']
    });
  }

  ngOnInit(): void {
    M.updateTextFields();
    
    var elems = document.querySelectorAll('select');
    var instances = M.FormSelect.init(elems);
  

    // this.addAuthorService.getAllAuthors().subscribe(authors => {
    //   console.log(authors);
    // });
  }

  public selectImage(event: any) {
    if (event.target.files.length > 0) {
      const file = event.target.files[0];
      this.image = file;
    }
    console.log("ISPIS");
    console.log(this.image);
  }

  public submitForm(data: any) {
    const formData = new FormData();
    formData.append('image', this.image);
    for (var value of formData.values()) {
      console.log(value);
    }
    formData.append('name', data.name);
    formData.append('nationality', data.nationality);
    formData.append('bio', data.bio);
    formData.append('born', data.born);
    formData.append('died', data.died);

    var nesto = this.addAuthorService.createImage(formData, data).subscribe((author: any) => {
      window.alert('Author is successfully created!');
      console.log(author);
    });
    this.checkoutForm.reset();
  }

}
