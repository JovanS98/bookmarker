import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Author } from '../models/author.model';

@Injectable({
  providedIn: 'root'
})
export class AddAuthorService {

  private readonly addAuthorUrl = 'http://localhost:3000/addAuthor/';

  constructor(private http: HttpClient) { }

  public createAuthor(data: any): Observable<Author>{
    const body = {...data};
    return this.http.post<Author>(this.addAuthorUrl, body);
  }

  public createImage(formData : any, data : any): Observable<any>{
    const body = {...data};
    return this.http.post<any>(this.addAuthorUrl, formData, body);
  }

  public getAllAuthors(){
    return this.http.get(this.addAuthorUrl);
  }
}
