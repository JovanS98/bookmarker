import { Component, Input, OnChanges, OnInit, Output, EventEmitter, ChangeDetectorRef, AfterViewInit, OnDestroy } from '@angular/core';
import { Datepicker } from 'materialize-css';
import * as moment from 'moment';
import { getDate, MOMENT_DATE_FORMAT } from 'src/app/util/utils';

export interface FilterField{
  fieldTitle: string,
  fieldName: string,
  type: "text"|"number"|"datetime"|"list",
  size?: "small"|"medium"|"full-width",
  currentValue?: string,
  icon?: string,
}

@Component({
  selector: 'app-filter-field',
  templateUrl: './filter-field.component.html',
  styleUrls: ['./filter-field.component.css']
})
export class FilterFieldComponent implements OnInit, OnChanges, AfterViewInit, OnDestroy {

  @Input() fieldInfo : FilterField;
  @Input() deletable? : boolean = true;
  @Output() fieldValueChangeEvent = new EventEmitter<FilterField>();
  classes: {};

  datepicker: Datepicker;
  // dateString: string;

  constructor() { 
  }

  ngOnInit(): void {
    if(!this.fieldInfo?.size){
      this.classes = {
        "small-filter-field" : true
      }
    }
    else{
      this.classes = {
        "small-filter-field": this.fieldInfo?.size === "small" ,
        "medium-filter-field": this.fieldInfo?.size === "medium" ,
        "full-width-filter-field": this.fieldInfo?.size === "full-width",
      }
    }
    this.classes = {...this.classes, "datepicker bookm-datepicker" : this.fieldInfo?.type === 'datetime'};
  }

  ngAfterViewInit(){
    this.datepicker = Datepicker.init(document.getElementById(this.fieldInfo.fieldName), {format:'dd.mm.yyyy'});
  }

  ngOnChanges(): void{
  }

  onKeypress(event: KeyboardEvent){
    // if(this.fieldInfo.type === 'datetime'){
    //   try{
    //     if(moment(this.dateString, DATE_FORMAT, true).isValid())
    //       this.datepicker.setDate(moment(this.dateString).toDate());
    //   } catch(err){
    //     console.log(err);
    //   }
    //   console.log(this.datepicker.date?.toISOString());
    // }
  }


  onFieldChange(event: Event): void {
    let value = (event.target as HTMLInputElement).value;
    if(this.fieldInfo.type==="datetime"){
      try{
        let dateValue = getDate(value);

        if(dateValue.isValid()){          
          this.datepicker.setDate(dateValue.toDate());
          this.datepicker.gotoDate(dateValue.toDate());
          // 
          this.fieldInfo = {
            ...this.fieldInfo,
            currentValue: dateValue.toISOString()
          }; 
          this.fieldValueChangeEvent.emit(this.fieldInfo);
        }
      } catch(err){
        console.log(err);
      }
    }
    else{
      this.fieldInfo = {
        ...this.fieldInfo,
        currentValue: value
      };    
      this.fieldValueChangeEvent.emit(this.fieldInfo);
    }
  }

  preventPickerOpening(event: Event){
    event.preventDefault();
    event.stopPropagation();
    event.stopImmediatePropagation();
  }

  onDatetimeClicked(){
    this.datepicker.open();
  }

  onDeleteClicked(){
    if(this.fieldInfo.type==='datetime'){
      this.datepicker.setDate(null);
      console.log(this.datepicker?.date);
    }
    this.fieldInfo = {
      ...this.fieldInfo,
      currentValue: undefined
    };
    this.fieldValueChangeEvent.emit(this.fieldInfo);
  }

  getCurrentValue(){
    if(this.fieldInfo.type==='datetime'){
      if(this.datepicker?.date == null){        // TODO: nece da obrise vrednost polja iako stavi da je date null... :)
        return "";
      }    
    }
    else if(this.fieldInfo.currentValue){
      return this.fieldInfo.currentValue;
    }
    return '';
  }

  ngOnDestroy(){
    this.datepicker.destroy();
  }

}
