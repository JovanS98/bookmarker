import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class BookService {

  //hardcoded path for now.
  private readonly bookUrl = 'http://localhost:3000/book/';
  private readonly reviewUrl = 'http://localhost:3000/review/';
  private readonly ratingUrl = 'http://localhost:3000/review/rating/';
  private readonly addRatingUrl = 'http://localhost:3000/review/add/';

  constructor(private http: HttpClient) { }

  public getBook(idBook: any) : Observable<any>{
    return this.http.get<any>(this.bookUrl + idBook);
  }

  public createReview(data : any, idBook: any) : Observable<any>{
    var body = {...data};
    return this.http.post<any>(this.reviewUrl + idBook, body);
  }
  
  public createRating(data : any, idBook: any) : Observable<any>{
    var body = {...data};
    return this.http.post<any>(this.addRatingUrl + idBook, body);
  }

  public getBookReviews(idBook: any) : Observable<any> {
    return this.http.get<any> (this.reviewUrl + idBook);
  }

}
