import mongoose from 'mongoose';

const connectDB = async() => {
    try{
        const conn = await mongoose.connect('mongodb+srv://marijaradovic:marija77998@cluster0.zhqmt.mongodb.net/Bookmarker?retryWrites=true&w=majority', {
            useNewUrlParser: true,
            useUnifiedTopology:true,
            useFindAndModify:false
        })

        console.log(`MongoDB connected: ${conn.connection.host}`)
    }

    catch(err){
        console.error(err);
        process.exit(1);
    }
} 

export default connectDB;