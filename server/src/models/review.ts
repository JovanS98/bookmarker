import mongoose,{Model, Document} from 'mongoose';
import {ObjectId} from 'mongodb';

import Book, { BookSearchI, IBookDocument, searchBooks } from './book';
import User, { IUserDocument } from './user';

export interface ReviewSearchI{
    book?: string,
    user?: string,
    title?: string,
    fromDate?: string,
    toDate?: string,
    keywords?: string[],
}


// document
export interface IReviewDocument extends Document {
    book: ObjectId | IBookDocument | any,
    reviews: {
        user: ObjectId | IUserDocument|any,
        rating: Number,
        postedAt: Date,
        title: String,
        comment: String
    }
}
// model
export interface IReviewModel extends Model<IReviewDocument> {
    // here we decalre statics
    addReview(bookId: any, userId: any, title: String, comment: String) : Promise<IReviewDocument> | undefined
    addRating(bookId: any, userId: any, rating: Number): Promise<IReviewDocument> | undefined
    getReviews(): any
    getAllReviews(bookId: any): Array<IReviewDocument>
    getUserReviewOfBook(bookId, userId): IReviewDocument
    getAverageRatingForBook(bookId): any
    getAverageRatingForUser(userId): any
    getAllUserReviews(userId): any
}

const bookReviewSchema = new mongoose.Schema({
    book: {
        type: ObjectId,
        ref: Book,
        required: true
    },
    reviews: {
        user: {
            type: ObjectId,
            ref: User,
            required: true
        },
        rating: {
            type: Number,
            required: true,
            default: 0
        },
        postedAt: {
            type: Date,
            default: Date.now
        },
        title: {
            type: String,
            default: ""
        },
        comment: {
            type: String,
            default: ""
        }
    }
});

// TODO Error in line 45, $addToSet
// bookReviewSchema.statics.addReview = async function(bookId, userId, rating, comment) {

//     // Check if the book exists
//     await Book.getBookById(bookId);

//     // If there is already a review for this book from this user, 
//     // then delete it and make a new one
//     await this.deleteOne({'book': bookId, 'ratings.user': userId});

//     // Add new review
//     await this.updateOne({'book': bookId}, {$addToSet: { 'ratings': { 'user': userId, 'rating': rating, 'comment': comment}}}).exec();

// }


bookReviewSchema.statics.addReview = async function (bookId, userId, title, comment) {
    const reviews = await this.find({ 'book': bookId });
    var exist;
    for (var i = 0; i < reviews.length; i++) {
        if (reviews[i].reviews.user == userId)
            exist = reviews[i];
    }

    if (!exist) {
        const review = new this({
            'book': bookId,
            'reviews': {
                'user': userId,
                'title': title,
                'comment': comment
            }
        });
        const savedReview = await review.save();

        return savedReview;
    }
    else {
        this.updateOne({ _id: exist._id }, { $set: { "reviews.title": title, "reviews.comment": comment, "reviews.postedAt" : Date.now()} }).exec();
    }
}

bookReviewSchema.statics.addRating = async function (bookId, userId, rating) {
    const reviews = await this.find({ 'book': bookId });
    var review;
    for (var i = 0; i < reviews.length; i++) {
        if (reviews[i].reviews.user == userId)
            review = reviews[i];
    }
    if (review) {
        this.updateOne({ _id: review._id }, { $set: { "reviews.rating": rating } }).exec();
    }
    else {
        const review = new this({
            'book': bookId,
            'reviews': {
                'user': userId
            }
        });

        const savedReview = await review.save();

        return savedReview;
    }
}

bookReviewSchema.statics.getReviews = async function () {
    const bookReviews = await this.find({})
        .populate('reviews.user', 'username')
        .exec();

    return bookReviews;
}

bookReviewSchema.statics.getAllReviews = async function (bookId) {
    const bookReviews = await this.find({ 'book': bookId })
        .populate('reviews.user', 'username')
        //.populate('book', 'title')
        .exec();

    return bookReviews;
}

bookReviewSchema.statics.getUserReviewOfBook = async function (bookId, userId) {
    const review = await this.findOne({ 'book': bookId, 'ratings.user': userId }, { 'ratings.$': 1, '_id': 0 }).exec();

    return review;
}

bookReviewSchema.statics.getAverageRatingForBook = async function (bookId) {
    await Book.getBookById(bookId);

    const avgRating = await this.aggregate([
        { $match: { book: mongoose.Types.ObjectId(bookId) } },
        { $project: { avgRating: { $avg: '$ratings.rating' } } }
    ]).exec();

    return avgRating;
}

bookReviewSchema.statics.getAverageRatingForUser = async function (userId) {
    await User.getUserById(userId);

    const avgRating = await this.aggregate([
        { $match: { 'rating.user': mongoose.Types.ObjectId(userId) } },
        { $project: { avgRating: { $avg: '$ratings.rating' } } }
    ]).exec();
}

bookReviewSchema.statics.getAllUserReviews = async function (userId) {
    await User.getUserById(userId);

    const reviews = await this.find({ 'reviews.user': userId }, { 'reviews.$': 1 })
        .populate('book', 'title')
        .exec();

    return reviews;
};


const BookReview = mongoose.model<IReviewDocument, IReviewModel>('BookReview', bookReviewSchema);


export async function searchReviews(options:ReviewSearchI) {
    let result : Array<object> = [];
    let criteriaOptions = {};
    if(options.book){ // bude id u dokumentu
        let bookIds = (await searchBooks({title: options.book} as BookSearchI)).map((book: IBookDocument) => book._id);
        criteriaOptions = {book: {$in: bookIds}};
    }
    if(options.title){
        criteriaOptions = {...criteriaOptions, "reviews.title": {$regex: options.title, $options: "i"}};
    }
    if(options.user){
        criteriaOptions = {...criteriaOptions, "reviews.user.username": {$regex: options.user, $options: "i"}};
    }
    if(options.keywords && options.keywords.length !== 0){
        criteriaOptions = {...criteriaOptions, $text: {$search: options.keywords.join(' ')}}
    }
    if(options.fromDate && options.toDate){ // postoje oba
        criteriaOptions = {...criteriaOptions, "reviews.postedAt": {
            $lt : new Date(options.toDate), $gte : new Date(options.fromDate)}};
    }
    else if(options.toDate){
        criteriaOptions = {...criteriaOptions, "reviews.postedAt": {$lt : new Date(options.toDate)}};
    }
    else if(options.fromDate){
        criteriaOptions = {...criteriaOptions, "reviews.postedAt": {$gte : new Date(options.fromDate)}};
    }
    
    (await BookReview.find(criteriaOptions).exec()).forEach(review=>{
        result.push(review);
    });
    return result;
}


export async function simpleSearchBookReview(keywords: string){
    let result: Array<object> = [];
    const indexName = "br_ss_comm_title_user";
    let indexes: object = await BookReview.collection.getIndexes();
    if(!Object.keys(indexes).includes(indexName)){
        await BookReview.collection.createIndex({
            "reviews.comment": "text",
            "reviews.title": "text",
            "reviews.user.username": "text"
        },{
            name: indexName
        });
    }

    (await BookReview.find({$text: {$search: keywords}})).forEach(bookReview=>{
        result.push(bookReview);
    });
    return result;
}

export default BookReview;