import mongoose, { Model, Document } from 'mongoose';
import {ObjectId} from "mongodb";
import Author from "./author";
import mongoosePaginate from 'mongoose-paginate-v2';

export interface BookSearchI{
    title?: string;
    author?: string;
    genre?: string[];
    keywords?: string[];

}

// document
export interface IBookDocument extends Document {
    title: String,
    author: any ,
    genre: any[],
    description: String,
    coverImagePath: String,
}
// model
export interface IBookModel extends Model<IBookDocument> {
    // here we decalre statics
    getBookById(bookId: String) : Promise<IBookDocument>
    getTitlesOfBooks(): Promise<Array<IBookDocument>>
    getAllBooks(): Promise<Array<IBookDocument>>
    getBooksByGenre(genre: any ): Promise<Array<IBookDocument>>
    getBooksByAuthor(author: any ): Promise<Array<IBookDocument>>
    getBooksByGenre(genre: any ): Promise<Array<IBookDocument>>
    addBook(title: String, author: any, genre: any, description: String, coverImagePath: String) : Promise<IBookDocument>
}

const bookSchema = new mongoose.Schema({
    title: {
        type: String,
        required: true
    },
    author: {
        type: ObjectId,
        ref: Author,
        required: true
    },
    genre: {
        type: Array,
        required: true
    },
    description: {
        type: String,
        required: true
    },
    coverImagePath: {
        type: String
    }
});

bookSchema.index({ title: 'text', description: 'text' });
bookSchema.plugin(mongoosePaginate);

bookSchema.statics.getBookById = async function(bookId: String) {
    const book = await this.findById(bookId)
    .populate('author');

    return book ? Promise.resolve(book) : Promise.reject();
}

bookSchema.statics.getTitlesOfBooks = async function(){
    const books = await this.find({},{'title': 1}).exec();

    return books;
}

bookSchema.statics.getAllBooks = async function() {
    const books = await this.find({}, { description: 0})
        .populate('author').exec();

    return books;
};

bookSchema.statics.getBooksByGenre = async function(genre) {
    const books = await this.find({ genre: genre }).exec();

    return books;
};

bookSchema.statics.getBooksByAuthor = async function(authorId) {
    const books = await this.find({ author: authorId }).exec();

    return books;
}

bookSchema.statics.addBook = async function(title, author, genre, description, coverImagePath) {


    const book = new this({
        'title' : title,
        'author': author,
        'genre': genre,
        'description': description,
        'coverImagePath': coverImagePath
    });

    

    const savedBook = await book.save(function(err:any, res:any){
        console.log("latest book id : ")
        console.log(res.id);
        Author.addAuthorsBook(res.author, res.id);
    });

    return savedBook;
};

const Book = mongoose.model<IBookDocument, IBookModel>('Book', bookSchema);//<IBookDocument, IBookModel>


export async function searchBooks(bookCriteria: BookSearchI, projection?: object) {
    let result : Array<object> = [];
    let criteriaOptions = {};
    if(bookCriteria.author){
        criteriaOptions = {author: {$regex : bookCriteria.author, $options:"i"}};
    }
    if(bookCriteria.genre){
        criteriaOptions = {...criteriaOptions, genre: {$in : bookCriteria.genre}};
    }
    if(bookCriteria.title){
        criteriaOptions = {...criteriaOptions, title: {$regex : bookCriteria.title, $options: "i"}};
    }
    if(bookCriteria.keywords){
        criteriaOptions = {...criteriaOptions, description: {$in : bookCriteria.keywords}};
    }
    
    (await Book.find(criteriaOptions, projection).exec()).forEach(book=>{
        result.push(book);
    });
    return result;
}


export async function simpleSearchBook(keywords: string){
    let result: Array<object> = [];
    const indexName = "b_ss_title_desc_author";
    let indexes: object = await Book.collection.getIndexes();

    if(!Object.keys(indexes).includes(indexName)){
        await Book.collection.createIndex({
            title: "text",
            description: "text",
            author: "text",
        }, {
            name: "b_ss_title_desc_author"
        });
    }
    (await Book.find({$text: {$search: keywords}})).forEach(book=>{
        result.push(book);
    });
    return result;
}

export default Book;
