import { Router } from "express";
import Review from '../models/review';
import { StatusCodes } from 'http-status-codes';
import User from '../models/user';

const reviewRouter = Router();

reviewRouter.get('/', async (req, res) => {
    try {
        const reviews = await Review.getReviews();
        res.status(StatusCodes.OK).json(reviews);
    } catch (err) {
        console.error("[err while posting review]", err);
        res.sendStatus(StatusCodes.NOT_FOUND);
    }
});

reviewRouter.post('/:id', async (req, res) => {
    try {
        await Review.addReview(req.params.id, req.body.userId, req.body.title, req.body.reviewContent);
    } catch (err) {
        console.error("[err while posting review]", err);
        res.sendStatus(StatusCodes.NOT_FOUND);
    }
});

reviewRouter.post('/add/:id', async (req, res) => {
    try {
        await Review.addRating(req.params.id, req.body.userId, req.body.rating);
    } catch (err) {
        console.error("[err while posting review]", err);
        res.sendStatus(StatusCodes.NOT_FOUND);
    }
});

reviewRouter.get('/:id', async (req, res) => {
    try {
        const id = req.params.id;
        const reviews = await Review.getAllReviews(id);
        res.status(StatusCodes.OK).json(reviews);
    }
    catch (err) {
        console.error("[err while geting book reviews]", err);
        res.sendStatus(StatusCodes.NOT_FOUND);
    }
});

reviewRouter.get('/user/:id', async (req, res) => {
    try {
        const id = req.params.id;
        const reviews = await Review.getAllUserReviews(id);
        res.status(StatusCodes.OK).json(reviews);
    }
    catch (err) {
        console.error("[err while geting book reviews]", err);
        res.sendStatus(StatusCodes.NOT_FOUND);
    }
});

reviewRouter.get('/following/:idUser', async (req, res) => {
    try {
        const user = await User.getUserById(req.params.idUser);
        const following = user.following;
        const arrayLength = following.length;

        if (arrayLength === 0) {
            console.log("ne prati nikoga");
            res.status(StatusCodes.OK).send([]);
        }
        else {
            const idFirstFollowing = user.following[0];
            const firstFollowing = await User.getUserById(idFirstFollowing);
            let reviews = await Review.getAllUserReviews(firstFollowing);
            //petlja
            for (let i = 1; i < arrayLength; i++) {
                let userWanted = await User.getUserById(following[i]);
                reviews = reviews.concat(await Review.getAllUserReviews(userWanted))
            }

            res.status(StatusCodes.OK).send(reviews);
        }
    }
    catch (err) {
        console.error("[err while geting all following reviews]", err);
        res.sendStatus(StatusCodes.NOT_FOUND);
    }
})

export default reviewRouter;