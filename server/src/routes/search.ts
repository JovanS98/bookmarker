import { Request, Response, Router } from "express";
import { StatusCodes } from 'http-status-codes';
import { AuthorSearchI, searchAuthors, simpleSearchAuthor } from "../models/author";
import { BookSearchI, searchBooks, simpleSearchBook } from "../models/book";
import { ReviewSearchI, searchReviews, simpleSearchBookReview } from "../models/review";
import { searchStories, simpleSearchStory, StorySearchI } from "../models/story";
import { searchUsers, simpleSearchUser, UserSearchI } from "../models/user";

const searchRouter = Router();


interface SearchI {
    user?: UserSearchI;
    book?: BookSearchI;
    author?: AuthorSearchI;
    story?: StorySearchI;
    review?: ReviewSearchI;
    simpleSearch?: SimpleSearchI;
}

interface SimpleSearchI {
    keywords?: string,
}

searchRouter.post('/', async (req: Request, res: Response)=>{
    
    let result : any[] = [];
    try{
        const searchCriteria: SearchI = req.body;
        if(searchCriteria.user){
            result = await searchUsers(searchCriteria.user);
        }
        if(searchCriteria.author){
            result = await searchAuthors(searchCriteria.author);
        }
        if(searchCriteria.book){
            result = await searchBooks(searchCriteria.book);
        }
        if(searchCriteria.review){
            result = await searchReviews(searchCriteria.review);
        }
        if(searchCriteria.story){
            result = await searchStories(searchCriteria.story);
        }
        if(searchCriteria.simpleSearch){
            let keywords = searchCriteria.simpleSearch.keywords; 
            if(keywords === undefined || keywords.length === 0){
                res.status(StatusCodes.BAD_REQUEST).send("Please enter the keywords for simple search");
                // return //? 
            }
            result = result.concat({author:await simpleSearchAuthor(keywords)})
            result = result.concat({user:await simpleSearchUser(keywords)})
            result = result.concat({book:await simpleSearchBook(keywords)})
            result = result.concat({review: await simpleSearchBookReview(keywords)})
            result = result.concat({story: await simpleSearchStory(keywords)})
        }
        res.status(StatusCodes.OK).send(result);
        
    } catch(err){
        console.error("[err search '/']", err);
        res.sendStatus(StatusCodes.NOT_FOUND);
    }
});

export default searchRouter;